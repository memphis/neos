# -------------------------------------------------------------------------
#
#  NEOS
#
#  -------------------------------------------------------------------------
#  License
#  This file is part of Neos.
#
#  Neos is free software: you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License v3 (LGPL)
#  as published by the Free Software Foundation.
#
#  Neos is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
#  License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with Neos. If not, see <http://www.gnu.org/licenses/>.
#
#---------------------------------------------------------------------------

import os

c.KernelGatewayApp.ip = '*'
c.KernelGatewayApp.port = 8080

c.KernelGatewayApp.env_process_whitelist = [
    'LD_LIBRARY_PATH',
    'LD_PRELOAD',
    'NSS_WRAPPER_PASSWD',
    'NSS_WRAPPER_GROUP',
]

image_config_file = '/opt/app-root/src/.jupyter/jupyter_kernel_gateway_config.py'

if os.path.exists(image_config_file):
    with open(image_config_file) as fp:
        exec(compile(fp.read(), image_config_file, 'exec'), globals())
