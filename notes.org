#+OPTIONS: ^:nil
#+OPTIONS: p:with-planning
* Environnement logiciel
| Langague   | C++                                          |
| Libs       | Bitpit, MPI, Lapacke, eigen3, petsc          |
| Build      | CMake                                        |
| Test       | GTest                                        |
| Projet     | https://gitlab.inria.fr/memphis/neos         |
| Couverture | https://sonarqube.bordeaux.inria.fr/         |
| Wrapper    | Python                                       |

* Gitlab
** dépôt
  - https://gitlab.inria.fr/memphis/neos

** CI
  - Runner: neos-docker (ci.inria.fr)
  - Image docker: hpclib/neos
  - Doc: https://gitlab.inria.fr/sed-bso/hpclib/blob/master/tuto/gitlab-ci-nojenkins.org
*** Configuration CI
   Pipeline en 4 étapes:
    - Compilation de la lib
    - Compilation des examples
    - Compilation/execution des tests et analyse statique du code
    - Transfert des infos sur sonarqube
   Ficher de configuration: .gitlab-ci.yml
#+begin_src sh
build-lib:
  stage: build-lib
  script:
    - git submodule init && git submodule update
    - mkdir build && cd build && cmake .. -DBUILD_SHARED_LIBS=ON -DBUILD_CORE=ON (.....)
    - make -j8
#+end_src
    - Résultats dans : https://gitlab.inria.fr/memphis/neos/pipelines

*** Sonarqube
    Regroupe les resultats de plusieurs logiciels:
    - cppcheck: Analyse statique C/C++
    - gcov, lcov: Couverture de code
    - rats: Audit de sécurité
    - clang analyser: Autre analyse statique de code
    - vera++: Vérifications de style de code sur du C++
    Fichier de configuration: sonar-project.properties
    Doc du SED: https://gitlab.inria.fr/sed-bso/hpclib/blob/master/tuto/sonarqube-dev.org
* Neos
** Core
   - Grille
   - Gestion des levelsets
#+begin_src c++
  Grid *grid = new Grid(-20, -20, -20, 50.0, 0.5, dim);
  LevelSet ls(grid);
  ASphere *geo  = new ASphere(10, 15, 10, 2.0, dim);
  ASphere *geo2 = new ASphere(5, 7, 13, 2.0, dim);
  ASphere *geo3 = new ASphere(7, 7, 3, 2.0, dim);

  ls.addGeometry(geo, "TAG");
  ls.addGeometry(geo2, "TAG");
  int id = ls.addGeometry(geo3);
  std::vector<double> phi = ls.getLevelSet("TAG");
#+end_src
   - Transport
#+begin_src c++
  Transport *trpt = new Transport(grid);
  ASphere	*geo    = new ASphere(0.5, 0.5, 0, 0.2, dim);

  trpt->compute(geo->getPhi(), speedVector, simuStep);
#+end_src
   - Interface
#+begin_src c++
  NeosInterface *neos = new NeosInterface();
  neos->createGrid({ {0., 0., 0.} }, 10, 0.2);
  neos->addSphere({ {5., 5., 5.} }, 2.0, "geo");
  neos->transport("geo", u, .5 / .3 * .8);
  neos->write();
#+end_src
   - Outils de conversion
** Geometry
   Incluse dans Neos:
    - Nuages de points:
      - Sphère
      - Cylindre
      - Cercle
    - Analytique:
      - Sphère
      - Cylindre
    - STL

  Une interface est fournie (Geometry.hpp)
  Hiérarchie de classe:
  - Geometry
    - AGeometry
      - ASphere
      - ACylinder
    - CPGeometry
      - 2DGeometry
        - Circle
      -3DGeometry
        - Sphere
        - Cylinder
    - STLGeometry
** Maths
   - Fonctions: Inversion de matrice, matrice vecteur...
   - Interpolation:
     - Polynomiales
     - RBF
     - Poids-distance (Floriant)
   - Laplacien:
     - Volumes finis
     - Differences finies
   - Gradient

   Pour les laplaciens et les interpolations, une factory retourne un pointeur sur "l'algo" voulu.
   Exemple pour les Laplaciens:
#+begin_src C++
  ILaplacian *lap = LaplacianFactory::get(lapType::FINITEVOLUME, solverType::PETSC, grille);
#+end_src
   Pour les Interpolations, on passe par un enum:
#+begin_src c++
  Interpolator interpo;
  double res = interpo.computeInterpolation(xpos, xref, val_ref, RBF);
  std::vector<double> weight = interpo->computeInterpolation(xpos, xref,POLY);
#+end_src
** Ajout un nouvel algo (exemple avec les interpoltions)
Dans src/maths/interpolator, créer la nouvelle classe et lui donner l'interface IInterpolator en héritage
(Les méthodes de l'interface devront obligatoirement etre implémentées):

#+begin_src C++
  class NewAlgo : public IInterpolator {
  public:
    double computeInterpolation(const NPoint &xpos, const std::vector<std::array<double,3> > &xref, const std::vector<double> &vref);
    std::vector<double> computeInterpolation(const NPoint &xpos, const std::vector<std::array<double,3> > &xref);
  };
#+end_src

Ajouter ensuite une nouvelle option dans le switch de la factory (InterpolatorFactory.cpp):
#+begin_src C++
    switch (itype)
    {
    case interpoType::NewAlog:
      interpo = new NewAlgo();
      break;
    case interpoType::POLY2D:
      interpo = new Polynomial2D();
      break;
      .....
#+end_src
Et completer l'enum dans src/includes/common.hpp

* Wrapper python
  Utilisation de Binder: https://github.com/RosettaCommons/binder
